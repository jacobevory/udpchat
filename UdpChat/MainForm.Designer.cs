﻿namespace UdpChat {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this._textBoxMain = new System.Windows.Forms.TextBox();
			this._textBoxSend = new System.Windows.Forms.TextBox();
			this._buttonSend = new System.Windows.Forms.Button();
			this.printDocument1 = new System.Drawing.Printing.PrintDocument();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._toolStripMenuItemSettings = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._toolStripMenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
			this._timerSpam = new System.Windows.Forms.Timer(this.components);
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// _textBoxMain
			// 
			this._textBoxMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._textBoxMain.BackColor = System.Drawing.Color.White;
			this._textBoxMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this._textBoxMain.Location = new System.Drawing.Point(3, 27);
			this._textBoxMain.Multiline = true;
			this._textBoxMain.Name = "_textBoxMain";
			this._textBoxMain.ReadOnly = true;
			this._textBoxMain.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this._textBoxMain.Size = new System.Drawing.Size(322, 220);
			this._textBoxMain.TabIndex = 0;
			// 
			// _textBoxSend
			// 
			this._textBoxSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._textBoxSend.Location = new System.Drawing.Point(3, 253);
			this._textBoxSend.Multiline = true;
			this._textBoxSend.Name = "_textBoxSend";
			this._textBoxSend.Size = new System.Drawing.Size(263, 64);
			this._textBoxSend.TabIndex = 1;
			this._textBoxSend.TextChanged += new System.EventHandler(this.OnTextChanged);
			// 
			// _buttonSend
			// 
			this._buttonSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this._buttonSend.Location = new System.Drawing.Point(272, 253);
			this._buttonSend.Name = "_buttonSend";
			this._buttonSend.Size = new System.Drawing.Size(53, 64);
			this._buttonSend.TabIndex = 2;
			this._buttonSend.Text = "Send";
			this._buttonSend.UseVisualStyleBackColor = true;
			this._buttonSend.Click += new System.EventHandler(this.OnClick);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this._toolStripMenuItemHelp});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(328, 24);
			this.menuStrip1.TabIndex = 3;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolStripMenuItemSettings,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "File";
			// 
			// _toolStripMenuItemSettings
			// 
			this._toolStripMenuItemSettings.Name = "_toolStripMenuItemSettings";
			this._toolStripMenuItemSettings.Size = new System.Drawing.Size(116, 22);
			this._toolStripMenuItemSettings.Text = "Settings";
			this._toolStripMenuItemSettings.Click += new System.EventHandler(this.OnMenuItemClick);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
			this.exitToolStripMenuItem.Text = "Exit";
			// 
			// _toolStripMenuItemHelp
			// 
			this._toolStripMenuItemHelp.Name = "_toolStripMenuItemHelp";
			this._toolStripMenuItemHelp.Size = new System.Drawing.Size(44, 20);
			this._toolStripMenuItemHelp.Text = "Help";
			this._toolStripMenuItemHelp.Click += new System.EventHandler(this.OnMenuItemClick);
			// 
			// _timerSpam
			// 
			this._timerSpam.Enabled = true;
			this._timerSpam.Interval = 500;
			this._timerSpam.Tick += new System.EventHandler(this.OnTimerTick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(328, 322);
			this.Controls.Add(this._buttonSend);
			this.Controls.Add(this._textBoxSend);
			this.Controls.Add(this._textBoxMain);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.ShowIcon = false;
			this.Text = "UDP Chat";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox _textBoxMain;
		private System.Windows.Forms.TextBox _textBoxSend;
		private System.Windows.Forms.Button _buttonSend;
		private System.Drawing.Printing.PrintDocument printDocument1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _toolStripMenuItemSettings;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _toolStripMenuItemHelp;
		private System.Windows.Forms.Timer _timerSpam;
	}
}

