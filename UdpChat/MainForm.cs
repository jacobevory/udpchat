﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace UdpChat {
	public partial class MainForm : Form {

		private readonly UdpClient udp = new UdpClient(22757);
		private IAsyncResult ar_ = null;
		private Thread t;
		private IPAddress broadcastMask = IPAddress.Parse("192.168.10.255");
		private List<IPAddress> ipList;
		private string lastMessage = String.Empty;
		private bool spam;
		private float fontSize = 9;

		public MainForm() {
			InitializeComponent();
			ipList = new List<IPAddress>();
			ipList.Add(broadcastMask);
			ipList.Add(IPAddress.Parse("192.168.7.255"));
			ipList.Add(IPAddress.Parse("192.168.1.255"));
			_textBoxMain.ForeColor = Color.Black;
			_textBoxMain.Text = "Welcome...";
			restart();
		}

		private void restart() {
			if (t != null && t.IsAlive) {
				t.Join(100);
			}
			if (t != null && t.IsAlive) {
				t.Abort();
			}
			t = new Thread(Receiver);
			ipList = new List<IPAddress>();
			ipList.Add(broadcastMask);
			ipList.Add(IPAddress.Parse("192.168.151.255"));
			ipList.Add(IPAddress.Parse("192.168.7.255"));
			ipList.Add(IPAddress.Parse("192.168.1.255"));
			t.Start();
			OnTextChanged(_textBoxSend, EventArgs.Empty);
			spam = false;
			setFont("Microsoft Sans Serif", fontSize);
		}

		private void setFont(string font, float fontsize) {
			_textBoxMain.Font = new Font(font, fontsize);
			_textBoxSend.Font = new Font(font, fontsize);
			_buttonSend.Font = new Font(font, 9.75f);
		}

		private void OnTextChanged(object sender, EventArgs e) {
			if (sender.Equals(_textBoxSend)) {
				if (String.IsNullOrWhiteSpace(_textBoxSend.Text)) {
					_buttonSend.Enabled = false;
					return;
				}
				_buttonSend.Enabled = true;
			}
		}

		private void OnClick(object sender, EventArgs e) {
			Send();
			_textBoxSend.Text = String.Empty;
		}

		private void Send() {
			if (_buttonSend.Enabled) {
				UdpClient client = new UdpClient();
				byte [] bytes = Encoding.ASCII.GetBytes(Encrypt(System.Environment.MachineName + ":" + System.Environment.UserName + ": " + _textBoxSend.Text));
				foreach (IPAddress ipAddress in ipList) {
					IPEndPoint ip = new IPEndPoint(ipAddress, 22757);
					client.Send(bytes, bytes.Length, ip);
					client.Send(bytes, bytes.Length, ip);
					client.Send(bytes, bytes.Length, ip);
				}
				client.Close();
			}
		}

		private string Encrypt(string input) {
			return input;
		}

		private string Decrypt(string input) {
			return input;
		}

		private void StartListening() {
			ar_ = udp.BeginReceive(Receive, new object());
		}

		private void Receive(IAsyncResult ar) {
			IPEndPoint ip = new IPEndPoint(IPAddress.Any, 22757);
			byte [] bytes = udp.EndReceive(ar, ref ip);
			string message = Encoding.ASCII.GetString(bytes);
			AppendTextBox(Decrypt(message));
			StartListening();
		}

		private void Receiver() {
			StartListening();
		}

		public void AppendTextBox(string value) {
			if (InvokeRequired) {
				this.Invoke(new Action<string>(AppendTextBox), new object [] { value });
				return;
			}

			if (value == lastMessage && spam) {
				return;
			}
			_textBoxMain.AppendText("\r\n" + value);
			lastMessage = value;
			spam = true;
		}

		protected override bool ProcessCmdKey(ref Message message, Keys keyData) {
			switch (keyData) {
				case (Keys.Enter):
					if (_textBoxSend.Focused) {
						OnClick(_buttonSend, EventArgs.Empty);
						return true;
					}
					break;
			}
			return false;
		}
		
		private void OnMenuItemClick(object sender, EventArgs e) {
			if (sender.Equals(_toolStripMenuItemSettings)) {
				var settings = new Settings(broadcastMask, fontSize);
				if (DialogResult.OK == settings.ShowDialog()) {
					if (settings.ipAddress != null) {
						broadcastMask = settings.ipAddress;
					}

					if (settings.fontSize > 0 && settings.fontSize < 100) {
						fontSize = settings.fontSize;
						setFont( "Microsoft Sans Serif", fontSize);
					}
				}
			} else if (sender.Equals(_toolStripMenuItemHelp)) {
				MessageBox.Show("There is no help. Figure it out ya dope.", "Nope.");
			}
		}

		private void OnTimerTick(object sender, EventArgs e) {
			spam = false;
		}
	}
}
