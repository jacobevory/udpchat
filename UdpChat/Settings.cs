﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace UdpChat {
	public partial class Settings : Form {

		public IPAddress ipAddress {
			get { if(IPAddress.TryParse(_textBoxMask.Text, out var newAddress)) {
				return newAddress;
				} return null;
			}
		}

		public float fontSize{
			get {
				return Convert.ToSingle(_numericUpDownFontSize.Value);
			}
		}



		public Settings(System.Net.IPAddress ip, float fontSize) {
			InitializeComponent();
			_textBoxMask.Text = ip.ToString();
			_numericUpDownFontSize.Value = Convert.ToDecimal(fontSize);
		}

		protected override bool ProcessCmdKey(ref Message message, Keys keyData) {
			switch (keyData) {
				case (Keys.Enter):
					DialogResult = DialogResult.OK;
					Close();
					return true;
			}
			return false;
		}
	}
}
